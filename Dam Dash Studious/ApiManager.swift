//
//  BBFApiManager.swift
//  AEOM
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class BBFApiManager
{
    static let sharedManager:BBFApiManager = BBFApiManager()
    func postDataWithJson(url:String,parameters:[String:[String:AnyObject]],completion:@escaping (_ responseDict:Any?,_ error:Error?,_ isDone:Bool)->Void)
    {
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: nil).responseJSON { response in
            do
            {
                let post:Any = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                completion(post,nil,true)
            }
            catch
            {
                completion(nil,response.result.error!,false)
            }
        }
    }
    func getDataWithJson(url:String,completion: @escaping(_ responseDict:Any?)-> Void)
    {
        Alamofire.request(url).responseJSON{
            response in
            if response.response != nil
            {
                if response.response?.statusCode == 200
                {
                    completion(response.result.value)
                }
            }
            else
            {
                completion(nil)
            }
        }
    }
}
