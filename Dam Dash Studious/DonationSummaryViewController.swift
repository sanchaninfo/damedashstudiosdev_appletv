//
//  BBFDonationSummaryViewController.swift
//  Black Box Faith
//
//  Created by Vijay Bhaskar on 02/05/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

class BBFDonationSummaryViewController: UIViewController {

    @IBOutlet var donationvalue: UILabel!
    @IBOutlet var dateofplace: UILabel!
    @IBOutlet var orderID: UILabel!
    @IBOutlet var continueShopBtn: UIButton!
    var summaryDict = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        orderID.text = "OrderID:\(String(summaryDict.value(forKey: "transactionid")as! Int))"
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date = dateformatter.date(from: (summaryDict.object(forKey: "date") as! String))
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/YYYY"
        dateofplace.text = "Donated on:  \(formatter.string(from: date!))"
        //dateofplace.text = "Date:\(summaryDict.value(forKey: "date") as! String)"
        donationvalue.text = "$ \(summaryDict.value(forKey: "amount") as! String)"

        // Do any additional setup after loading the view.
    }
    @IBAction func Continue(_ sender: Any) {
        for viewcontroller in (self.navigationController?.viewControllers)!
        {
            if viewcontroller.isKind(of:BBFPlayerLayerViewController.self)
            {
                let  _ =  self.navigationController?.popToViewController(viewcontroller, animated: false)
            }
        }
        
    }
    
  
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView == continueShopBtn
        {
            continueShopBtn.layer.borderWidth = 5.0
            continueShopBtn.layer.borderColor = focusColor
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
